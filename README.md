# README #

This repository is created for storing and viewing of interview coding tasks.

### How do I get set up? ###

Please clone this repo on your workstation and open the solution file with Visual Studio. Then setup a proper "Start Up Project" and run.

### Contribution guidelines ###

This is read-only repo. The only contributor is Alexander Oreshkov.