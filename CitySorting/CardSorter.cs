﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace CitySorting
{
    /// <summary>
    /// Card collection sorting class
    /// </summary>
    public class CardSorter
    {
        private class OneDirectionLink<T> : IEnumerable<T> where T : class
        {
            public OneDirectionLink(T value)
            {
                Value = value;
            }

            private OneDirectionLink<T> RightNode { get; set; }

            private T Value { get; }

            public void LinkTo(OneDirectionLink<T> rightNode)
            {
                RightNode = rightNode;
            }

            public IEnumerator<T> GetEnumerator()
            {
                var currentNode = this;
                while (currentNode != null)
                {
                    yield return currentNode.Value;
                    currentNode = currentNode.RightNode;
                }
            }

            public override string ToString()
            {
                return Value?.ToString() ?? string.Empty;
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }
        }

        /// <summary>
        /// Sorting  method  with complexity O(n)
        /// </summary>
        /// <param name="unsortedCardsCollection"></param>
        /// <returns></returns>
        public static IList<Card> Sort(IList<Card> unsortedCardsCollection)
        {
            if (unsortedCardsCollection == null)
                throw new ArgumentNullException();

            if (!unsortedCardsCollection.Any())
                return unsortedCardsCollection;

            var departureCityList = new Dictionary<string, OneDirectionLink<Card>>();
            var arrivalCityList = new Dictionary<string, OneDirectionLink<Card>>();

            foreach (var currentCard in unsortedCardsCollection)
            {
                var currentArrivalCity = currentCard.ArrivalCity;
                var currentDepartureCity = currentCard.DepartureCity;

                var currentCardLinked = new OneDirectionLink<Card>(currentCard);

                var hasDepartureCardToLink = departureCityList.ContainsKey(currentArrivalCity);
                var hasArrivalCardToLink = arrivalCityList.ContainsKey(currentDepartureCity);

                if (hasDepartureCardToLink)
                {
                    var departureToCurrentCard = departureCityList[currentArrivalCity];
                    departureToCurrentCard.LinkTo(currentCardLinked);
                    departureCityList.Remove(currentArrivalCity);
                }
                else
                {
                    arrivalCityList.Add(currentArrivalCity, currentCardLinked);
                }

                if (hasArrivalCardToLink)
                {
                    var arrivalFromCurrentCard = arrivalCityList[currentDepartureCity];
                    currentCardLinked.LinkTo(arrivalFromCurrentCard);
                    arrivalCityList.Remove(currentDepartureCity);
                }
                else
                {
                    departureCityList.Add(currentDepartureCity, currentCardLinked);
                }
            }

            if (departureCityList.Count > 1 || arrivalCityList.Count > 1)
                throw new ArgumentException("Unable to sort collection. Could not build a continuous route.");

            var currentNode = arrivalCityList.Values.First();
            return new List<Card>(currentNode);
        }
    }
}