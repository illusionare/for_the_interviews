﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CitySorting
{
    class Program
    {
        public const string City1 = "City1";
        public const string City2 = "City2";
        public const string City3 = "City3";
        public const string City4 = "City4";
        public const string City5 = "City5";
        public const string City6 = "City6";
        public const string City7 = "City7";

        static void Main(string[] args)
        {
            List<Card> unsortedaCardsCollection = new List<Card>()
            {
                new Card(City3, City4),
                new Card(City2, City3),
                new Card(City6, City7),
                new Card(City4, City5),
                new Card(City1, City2),
                new Card(City5, City6)
            };

            var sortedCards = CardSorter.Sort(unsortedaCardsCollection).ToList();

            sortedCards.ForEach(card=> Console.WriteLine("{0}=>{1}", card.ArrivalCity, card.DepartureCity));

            Console.ReadLine();

        }
    }

}
