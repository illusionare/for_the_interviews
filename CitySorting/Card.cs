﻿namespace CitySorting
{
    public class Card
    {
        public string ArrivalCity { get; }
        public string DepartureCity { get; }

        public Card(string arrivalCity, string departureCity)
        {
            ArrivalCity = arrivalCity;
            DepartureCity = departureCity;
        }

        public override string ToString()
        {
            return $"{ArrivalCity}=>{DepartureCity}";
        }
    }
}