﻿using System;
using System.Collections.Generic;
using CitySorting;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestsProject
{
    [TestClass()]
    public class CardSorterTests
    {
        public const string Melburn = "Мельбурн";
        public const string Koln = "Кельн";
        public const string Moscow = "Москва";
        public const string Paris = "Париж";


        [TestMethod()]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ArgumentNullTest()
        {
            CardSorter.Sort(null);
        }

        [TestMethod()]
        public void ArgumentEmptyTest()
        {
            var unsortedCardsCollection = new List<Card>();

            var sorted = CardSorter.Sort(unsortedCardsCollection);

            Assert.AreEqual(sorted, unsortedCardsCollection);
        }


        [TestMethod()]
        public void ArgumentTestUnSorted()
        {
            var unsortedCardsCollection = new List<Card>()
            {
                new Card(Melburn, Koln),
                new Card(Moscow, Paris),
                new Card(Koln, Moscow)
            };

            var sorted = CardSorter.Sort(unsortedCardsCollection);

            bool sorderFlag = true;
            for (int i = 0; i < unsortedCardsCollection.Count-1; i++)
            {
                sorderFlag &= sorted[i].DepartureCity == sorted[i + 1].ArrivalCity;
            }

            Assert.IsTrue(sorderFlag);
        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentException))]
        public void ArgumentTestUnSortable()
        {
            var unsortedCardsCollection = new List<Card>()
            {
                new Card(Moscow, Paris),
                new Card(Koln, Melburn)
            };

            CardSorter.Sort(unsortedCardsCollection);
        }


    }
}